package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private List<String> pObstacles;
	private List<String> foundObstacles = new ArrayList<>();
	private boolean obstacleFound;
	private int x_size;
	private int y_size;
	private int current_x;
	private int current_y;
	private String dir;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		set_pObstacles(planetObstacles);
		set_x_size(planetX);
		set_y_size(planetY);
		set_current_x(0);
		set_current_y(0);
		set_dir("N");
		setFoundObstacles(new ArrayList<>());
		setObstacleFound(false);
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {

		for(int i = 0; i < get_pObstacles().size(); i++) {
		
			if (get_pObstacles().get(i).equals("(" + x + "," + y + ")")) {
				return true;
			}
		}
			
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		for (int x = 0; x < commandString.length(); x++) {
				
			if (commandString.charAt(x) == 'f') {
					
				if (get_dir().equals("N")) {
					
					if (planetContainsObstacleAt(get_current_x(), get_current_y() + 1)) {
						getFoundObstacles().add("(" + get_current_x() + "," + (get_current_y() + 1) + ")");
						setObstacleFound(true);
						
					} else if (get_current_y() < get_y_size() - 1) {
						set_current_y(get_current_y() + 1);
						
					} else {
						set_current_y(0);
					}
		
				} else if (get_dir().equals("S")) {
					
					if (planetContainsObstacleAt(get_current_x(), get_current_y() - 1)) {
						getFoundObstacles().add("(" + get_current_x() + "," + (get_current_y() - 1) + ")");
						setObstacleFound(true);
						
					} else if (get_current_y() > 0) {
						set_current_y(get_current_y() - 1);
						
					} else {
						set_current_y(get_y_size()-1);
					}
							
				} else if (get_dir().equals("E")) {
					
					if (planetContainsObstacleAt(get_current_x() + 1, get_current_y())) {
						getFoundObstacles().add("(" + (get_current_x() + 1) + "," + get_current_y() + ")");
						setObstacleFound(true);
						
					} else if (get_current_x() < get_x_size() - 1) {
						set_current_x(get_current_x() + 1);
						
					} else {
						set_current_x(0);
					}
	
				} else if (get_dir().equals("W")) {
					
					if (planetContainsObstacleAt(get_current_x() - 1, get_current_y())) {
						getFoundObstacles().add("(" + (get_current_x() - 1) + "," + get_current_y() + ")");
						setObstacleFound(true);
						
					} else if (get_current_x() > 0) {
						set_current_x(get_current_x() - 1);
						
					} else {
						set_current_x(get_x_size()-1);
					}
					
				}
					
			} else if (commandString.charAt(x) == 'b') {
					
				if (get_dir().equals("N")) {
					
					if (planetContainsObstacleAt(get_current_x(), get_current_y() - 1)) {
						getFoundObstacles().add("(" + get_current_x() + "," + (get_current_y() - 1) + ")");
						setObstacleFound(true);
						
					} else if(get_current_y() > 0) {
						set_current_y(get_current_y() - 1);
						
					} else {
						set_current_y(get_y_size()-1);
						
					}
					
				} else if (get_dir().equals("S")) {
					
					if (planetContainsObstacleAt(get_current_x(), get_current_y() + 1)) {
						getFoundObstacles().add("(" + get_current_x() + "," + (get_current_y() + 1) + ")");
						setObstacleFound(true);
						
					} else if(get_current_y() < get_y_size()-1) {
						set_current_y(get_current_y() + 1);
						
					} else {
						set_current_y(0);
					}
	
				} else if (get_dir().equals("E")) {
					
					if (planetContainsObstacleAt(get_current_x() - 1, get_current_y())) {
						getFoundObstacles().add("(" + (get_current_x() - 1) + "," + get_current_y() + ")");
						setObstacleFound(true);
						
					} else if (get_current_x() > 0) {
						set_current_x(get_current_x() - 1);
						
					} else {
						set_current_x(get_x_size()-1);
					}
		
				} else if (get_dir().equals("W")) {
					
					if (planetContainsObstacleAt(get_current_x() + 1, get_current_y())) {
						getFoundObstacles().add("(" + (get_current_x() + 1) + "," + get_current_y() + ")");
						setObstacleFound(true);
						
					} else if (get_current_x() < get_x_size()-1) {
						set_current_x(get_current_x() + 1);
						
					} else {
						set_current_x(0);
					}
				}
					
			} else if (commandString.charAt(x) == 'r') {
					
				if (get_dir().equals("N")) {
					set_dir("E");
						
				} else if (get_dir().equals("S")) {
					set_dir("W");
						
				} else if (get_dir().equals("W")) {
					set_dir("N");
						
				} else if (get_dir().equals("E")) {
					set_dir("S");
				}
					
			} else if (commandString.charAt(x) == 'l') {
					
				if (get_dir().equals("N")) {
					set_dir("W");
						
				} else if (get_dir().equals("S")) {
					set_dir("E");
						
				} else if (get_dir().equals("W")) {
					set_dir("S");
						
				} else if (get_dir().equals("E")) {
					set_dir("N");
				} 
			}
		}
		
		if (obstacleIsFound()) {
			return "(" + get_current_x() + "," + get_current_y() + "," + get_dir() + ")" + getFoundObstacles().get(0);
			
		} 
			
		return "(" + get_current_x() + "," + get_current_y() + "," + get_dir() + ")";
	}
	
	private List<String> get_pObstacles() {
		return pObstacles;
	}
	
	private void set_pObstacles(List<String> planetObstacles) {
		pObstacles = planetObstacles;
	}
	
	private int get_x_size() {
		return x_size;
	}
	
	private void set_x_size(int x) {
		x_size = x;
	}
	
	private int get_y_size() {
		return y_size;
	}
	
	private void set_y_size(int y) {
		y_size = y;
	}
	
	private int get_current_x() {
		return current_x;
	}
	
	private void set_current_x(int x) {
		current_x = x;
	}

	private int get_current_y() {
		return current_y;
	}
	
	private void set_current_y(int y) {
		current_y = y;
	}
	
	private String get_dir() {
		return dir;
	}
	
	private void set_dir(String direction) {
		dir = direction;
	}
	
	private boolean obstacleIsFound() {
		return obstacleFound;
	}
	
	private void setObstacleFound(boolean obsFound) {
		obstacleFound = obsFound;
	}
	
	private List<String> getFoundObstacles() {
		return foundObstacles;
	}
	
	private void setFoundObstacles(List<String> foundObs) {
		foundObstacles = foundObs;
	}
}
