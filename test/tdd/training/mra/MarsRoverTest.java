package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetContainsObstacleAt() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		assertTrue(marsRover.planetContainsObstacleAt(2, 3));
	}
	
	@Test
	public void testExecuteCommandEmptyString() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,N)", marsRover.executeCommand(""));
	}
	
	@Test
	public void testExecuteCommandRightLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,E)", marsRover.executeCommand("r"));
	}
	
	@Test
	public void testExecuteCommandMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,1,N)", marsRover.executeCommand("f"));
	}
	
	@Test
	public void testExecuteCommandMovingBackwards() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("r");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		assertEquals("(3,3,E)", marsRover.executeCommand("b"));
	}
	
	@Test
	public void testExecuteCommandMovingCombined() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("r");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		assertEquals("(1,6,E)", marsRover.executeCommand("bblfffrb"));
	}
	
	@Test
	public void testExecuteCommandWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,9,N)", marsRover.executeCommand("b"));
	}
	
	@Test
	public void testExecuteCommandWrapping2() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(9,9,E)", marsRover.executeCommand("brb"));
	}
	
	@Test
	public void testExecuteCommandSingleObstacleEncounter() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(1,2,E)(2,2)", marsRover.executeCommand("ffrfff"));
	}
	
}
